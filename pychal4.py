# http://www.pythonchallenge.com/pc/def/equality.html
import re

file = open("datachal4", "r")
data = []
pattern = '[a-z][A-Z]{3}[a-z][A-Z]{3}[a-z]'
data = file.read()
found = re.findall(pattern, data)
message = ''.join([found[4] for found in found])
print message