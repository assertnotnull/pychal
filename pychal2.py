class Decoder(object):
    def decode(self, string):
        resultat = ""
        for letter in string:
            if letter.isalpha():
                num = ord(letter)
                if num >= 121:
                    resultat += chr(num - 26 +2)
                else:
                    resultat += chr(num + 2)
            else:
                resultat += letter
        print resultat
        return resultat

if __name__ == "__main__":
    texte = "g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. bmgle gr gl zw fylb gq glcddgagclr ylb   rfyr'q ufw rfgq rcvr gq qm jmle. sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj."
    print texte

    url = "http://www.pythonchallenge.com/pc/def/map.html"
