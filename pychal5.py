# http://www.pythonchallenge.com/pc/def/linkedlist.php
from urllib2 import urlopen
import re

pattern = '\w*\s*(?P<number>[0-9]+)'

def navigate(id, times):
    url = 'http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing='
    data = urlopen(url+id)
    string = data.read()
    matches = re.findall(pattern, string)
    if len(matches) is not 0:
        number = matches[len(matches)-1]
        print number
        navigate(number, times-1)
    else:
        print string
        if re.search('two', string) is not None:
            navigate(str(int(id)/2), times-1)
        if re.search('previous', string) is not None:
            print string, id, times

start = '12345'
navigate(start, 400)
